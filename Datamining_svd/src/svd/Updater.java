package svd;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import common.Retriever;


/*In this class we get the reduced matrices from ReducedData.java and we manipulate them with
 *  our update rules
 */

public class Updater {

	private ReducedData reducedData;
	private final double LRATE = 0.001;
	private final int NUM_RATINGS = 90570;
	private final int NUM_USERS = 944;
	private final int NUM_ITEMS = 1683;


	public Updater(ReducedData newReducedData){
		this.reducedData = newReducedData;

	}



	//Useful to actually select values appearing in the training set
	public boolean hasRated(Integer user, Integer item){

		List<Object> parsedData = reducedData.getParsedData();

		Map<Integer, HashMap<Integer, Integer>> usersMap = (Map<Integer, HashMap<Integer, Integer>>)parsedData.get(0);


		if(usersMap.containsKey(user)){

			if(usersMap.get(user).containsKey(item)){

				return true;

			}

		} 

		return false;

	}


	//Dot product between 2 arrays of the same size
	public double dotProduct(ArrayList<Double> array1, ArrayList<Double> array2){

		double dotProduct = 0;

		for(int i = 0; i < array1.size(); i++) {

			dotProduct += array1.get(i).doubleValue() * array2.get(i).doubleValue();

		}

		return dotProduct;

	}

	//Addition of 2 arrays of the same size
	public ArrayList<Double> arrayAddition(ArrayList<Double> array1, ArrayList<Double> array2){

		ArrayList<Double> result = new ArrayList<Double>();

		for(int i = 0; i < array1.size(); i++){

			result.add(i, array1.get(i).doubleValue() + array2.get(i).doubleValue());

		}	

		return result;

	}

	//multiplication of array by a constant number
	public ArrayList<Double> arrayMultByConstant(ArrayList<Double> array, double constant){

		ArrayList<Double> result  = new ArrayList<Double>();

		for(int i = 0; i < array.size(); i++){

			result.add(i, array.get(i).doubleValue()*constant);

		}

		return result;

	}

	//get the value of a vector
	public double getVectorValue(ArrayList<Double> vector){

		double value = 0;

		for(int i = 0; i < vector.size(); i++){

			value += vector.get(i).doubleValue()*vector.get(i).doubleValue();

		}

		return value;

	}


	//gets the sum of square error, then we can obtain the cost function taking the minimum
	public double getCostFunction(HashMap<Integer, ArrayList<Double>> usersMatrix, HashMap<Integer, ArrayList<Double>> itemsMatrix){

		Retriever r = new Retriever(reducedData.getParsedData());

		double sum = 0;

		Map<Integer, HashMap<Integer, Integer>> usersMap = (Map<Integer, HashMap<Integer, Integer>>)reducedData.getParsedData().get(0);

		for(Entry<Integer, ArrayList<Double>> entry : usersMatrix.entrySet()){

			int user = entry.getKey();
			ArrayList<Double> userValues = entry.getValue();

			for(Entry<Integer, ArrayList<Double>> entry2 : itemsMatrix.entrySet()){

				int item = entry2.getKey();
				ArrayList<Double> itemValues = entry2.getValue();

				if(hasRated(user, item)){  //if the couple (user, item) exists, we proceed to calculations

					sum += (r.getUserRatingToItem(usersMap, user, item) - dotProduct(userValues, itemValues)) * (r.getUserRatingToItem(usersMap, user, item) - dotProduct(userValues, itemValues));


				}

			}

		}

		return sum;
	}


	public void updateValues(){

		Retriever retriever = new Retriever(reducedData.getParsedData());

		double costFunction = 0;
		int iterations = 0;
		
		List<Ligne> listLignesTraining = (List<Ligne>) reducedData.getParsedData().get(4);
		List<Ligne> listLignesTest = (List<Ligne>) reducedData.getParsedData().get(5);
		
		LinkedHashMap<Integer, ArrayList<Double>> usersMatrix = reducedData.getUsersMatrix();
		LinkedHashMap<Integer, ArrayList<Double>> itemsMatrix = reducedData.getItemsMatrix();


		ArrayList<Integer> usersOfTrainingSet = retriever.getAllUsersFromTrainingSet();
		ArrayList<Integer> itemsOfTrainingSet = retriever.getAllItemsFromTrainingSet();
		
		ArrayList<Integer> users = usersOfTrainingSet;
		ArrayList<Integer> items = itemsOfTrainingSet;

		//Here we get our randomly filled reduced matrices
		usersMatrix = reducedData.getReducedUsersMatrix(users, 10); 
		itemsMatrix = reducedData.getReducedItemsMatrix(items, 10);

		//System.out.println(itemsMatrix);

		Map<Integer, HashMap<Integer, Integer>> usersMap = (Map<Integer, HashMap<Integer, Integer>>)reducedData.getParsedData().get(0);

		/*int[][][] preferences = new int[NUM_USERS][NUM_ITEMS][NUM_RATINGS];
		int[] usersTab = new int[NUM_USERS];
		int[] itemsTab = new int[NUM_ITEMS];*/

		//Iterations fixed randomly to see what happens, of course we can put a condition on the costFunction later on
		while(iterations <= 50){

			Collections.shuffle(listLignesTraining);
			
			
			//System.out.println(listLignesTraining);
			
			LinkedHashMap<Integer, ArrayList<Double>> newUsersMatrix = new LinkedHashMap<Integer, ArrayList<Double>>();
			LinkedHashMap<Integer, ArrayList<Double>> newItemsMatrix = new LinkedHashMap<Integer, ArrayList<Double>>();
			
			for(Ligne l : listLignesTraining) {
				
				int user = l.getUser();
				//System.out.println(user);
				ArrayList<Double> userValues = usersMatrix.get(user);

				int item = l.getItem();
				//System.out.println(item);
				ArrayList<Double> itemValues = itemsMatrix.get(item);
				
				//ArrayList<Double> currentUserValue = userValues; //Value line associated to the user in the user matrix
				//ArrayList<Double> currentItemValue = itemValues; //Value line associated to the item in the item matrix

				//defining the error : actual rating - predicted rating
				double error = LRATE * (l.getRating() - dotProduct(itemValues, userValues));
				
				/*
				if(listLignesTraining.indexOf(l) == 0) {
					System.out.println(userValues);
					System.out.println(itemValues);
					
					System.out.println(error);
				}
				*/

				//Update rules
				ArrayList<Double> tempValueItems = arrayAddition(arrayMultByConstant(itemValues, 2), arrayMultByConstant(userValues, error));
				ArrayList<Double> tempValueUsers = arrayAddition(arrayMultByConstant(userValues, 2), arrayMultByConstant(itemValues, error));

				newUsersMatrix.put(user, tempValueUsers);
				newItemsMatrix.put(item, tempValueItems);
			}

			/*
			for(Entry<Integer, ArrayList<Double>> entry : usersMatrix.entrySet()){

				int user = entry.getKey();
				ArrayList<Double> userValues = entry.getValue();

				for(Entry<Integer, ArrayList<Double>> entry2 : itemsMatrix.entrySet()){

					int item = entry2.getKey();
					ArrayList<Double> itemValues = entry2.getValue();

					if(hasRated(user, item)){ //if the couple (user, item) exists, we proceed to calculations
						
						ArrayList<Double> currentUserValue = userValues; //Value line associated to the user in the user matrix
						ArrayList<Double> currentItemValue = itemValues; //Value line associated to the item in the item matrix

						ArrayList<Double> tempValueItems = new ArrayList<Double>(); //To make the operations simultaneous, we should keep an extra copy of the current item value line 
						for(int i = 0; i < itemValues.size(); i++)
							tempValueItems.add(i, itemValues.get(i));  //deep copy

						ArrayList<Double> tempValueUsers = new ArrayList<Double>(); //To make the operations simultaneous, we should keep an extra copy of the current user value line 
						for(int i = 0; i < userValues.size(); i++)
							tempValueUsers.add(i, userValues.get(i));  //deep copy

						//defining the error : actual rating - predicted rating
						double error = LRATE * (retriever.getUserRatingToItem(usersMap, user, item) - dotProduct(tempValueItems, tempValueUsers));

						//Update rules
						currentItemValue = arrayAddition(arrayMultByConstant(tempValueItems, 2), arrayMultByConstant(tempValueUsers, error));
						currentUserValue = arrayAddition(arrayMultByConstant(tempValueUsers, 2), arrayMultByConstant(tempValueItems, error));

						newUsersMatrix.put(user, currentUserValue);
						newItemsMatrix.put(item, currentItemValue);
					}

				}

			}
			*/

			costFunction = getCostFunction(newUsersMatrix, newItemsMatrix);
			System.out.println("Cost Function : " + costFunction);

			//Storing the new Values inside our matrices for the next iteration
			usersMatrix = new LinkedHashMap<Integer, ArrayList<Double>>(newUsersMatrix);
			itemsMatrix = new LinkedHashMap<Integer, ArrayList<Double>>(newItemsMatrix);

			iterations++;
			
			

		}

		this.reducedData.setItemsMatrix(itemsMatrix);
		this.reducedData.setUsersMatrix(usersMatrix);

		/*Comparing an actual rates with a predicted rate*/
		//int rate = retriever.getUserRatingToItem(usersMap, 166, 346);
		//double rate2 = dotProduct(usersMatrix.get(166), itemsMatrix.get(346));


	}


	public ReducedData getReducedData() {
		return reducedData;
	}



	public void setReducedData(ReducedData reducedData) {
		this.reducedData = reducedData;
	}




}
