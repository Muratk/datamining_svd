package svd;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import common.Parser;

public class Implementation {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		
		
		LinkedHashMap<Integer, ArrayList<Double>> usersMatrix = null;
		LinkedHashMap<Integer, ArrayList<Double>> itemsMatrix = null;
		LinkedHashMap<Integer, ArrayList<Double>> updatedUsersMatrix = null;
		LinkedHashMap<Integer, ArrayList<Double>> updatedItemsMatrix = null;
		Parser parser = new Parser("u.data");
		
		List<Object> usersData = null;
		
		usersData = parser.parseData(1);
		
		ReducedData reducedData = new ReducedData(usersMatrix, itemsMatrix, usersData);
		
		
		Updater updater = new Updater(reducedData);
		
		updater.updateValues();
		
		updatedItemsMatrix = updater.getReducedData().getItemsMatrix();
		updatedUsersMatrix = updater.getReducedData().getUsersMatrix();
		
		
		/*System.out.println(updater.getUsersMatrix());
		System.out.println(updater.getItemsMatrix());*/

		
	}

}
