package svd;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;


/*In this class we produce random matrices for all users of the training set
 * and all items of the training set
 * Then we fill this matrices with random values according to the chosen number of features
 */

public class ReducedData {

	Random randomNumber = new Random();

	private LinkedHashMap<Integer, ArrayList<Double>> usersMatrix;
	private LinkedHashMap<Integer, ArrayList<Double>> itemsMatrix;
	private List<Object> parsedData;

	public ReducedData(LinkedHashMap<Integer, ArrayList<Double>> newUsersMatrix, LinkedHashMap<Integer, ArrayList<Double>> newItemsMatrix, List<Object> parsedData){

		this.usersMatrix = newUsersMatrix;
		this.itemsMatrix = newItemsMatrix;
		this.parsedData = parsedData;
		
	}
 
	//Some getters and setters*******************************//

	public List<Object> getParsedData() {
		return parsedData;
	}


	public void setParsedData(
			List<Object> parsedData) {
		this.parsedData = parsedData;
	}

	
	public LinkedHashMap<Integer, ArrayList<Double>> getUsersMatrix() {
		return usersMatrix;
	}

	public void setUsersMatrix(LinkedHashMap<Integer, ArrayList<Double>> usersMatrix) {
		this.usersMatrix = usersMatrix;
	}

	public LinkedHashMap<Integer, ArrayList<Double>> getItemsMatrix() {
		return itemsMatrix;
	}

	public void setItemsMatrix(LinkedHashMap<Integer, ArrayList<Double>> itemsMatrix) {
		this.itemsMatrix = itemsMatrix;
	}
	
	//***********************************//
	
	public LinkedHashMap<Integer, ArrayList<Double>> getReducedUsersMatrix(ArrayList<Integer> users, int numberOfFeatures){

		usersMatrix = new LinkedHashMap<Integer, ArrayList<Double>>();
		
		ArrayList<Double> randomValues = null;

		//Filling every users with random values

		for(int j = 0; j < users.size(); j++) {
			
			randomValues = new ArrayList<Double>();
			Integer currentUser = users.get(j).intValue();
			
			for(int i = 0; i < numberOfFeatures; i++){

				double randomValue = randomNumber.nextDouble() - 0.5; //0.1 is the minimum, 0.9 is the maximum
				randomValues.add(randomValue);

			}
		
			usersMatrix.put(currentUser, randomValues);

		}
			
		return usersMatrix;
		
	}
	
	
	//Exactly the same method with Items this time
	public LinkedHashMap<Integer, ArrayList<Double>> getReducedItemsMatrix(ArrayList<Integer> items, int numberOfFeatures){

		itemsMatrix = new LinkedHashMap<Integer, ArrayList<Double>>();
		
		ArrayList<Double> randomValues = null;

		//Filling every items with random values

		for(int j = 0; j < items.size(); j++) {
			
			randomValues = new ArrayList<Double>();
			Integer currentItem = items.get(j).intValue();
			
			for(int i = 0; i < numberOfFeatures; i++){

				double randomValue = randomNumber.nextDouble() - 0.5; //0.01 is the minimum, 0.99 is the maximum
				randomValues.add(randomValue);

			}
			
			itemsMatrix.put(currentItem, randomValues);
			
		}	
	
		return itemsMatrix;
		
	}


}
