package svd;

public class Ligne {

	int user;
	int item;
	int rating;
	
	public Ligne(int user, int item, int rating) {
		
		this.user = user;
		this.item = item;
		this.rating = rating;
	}

	public int getUser() {
		return user;
	}

	@Override
	public String toString() {
		return "Ligne [user=" + user + ", item=" + item + ", rating=" + rating
				+ "]";
	}

	public void setUser(int user) {
		this.user = user;
	}

	public int getItem() {
		return item;
	}

	public void setItem(int item) {
		this.item = item;
	}

	public int getRating() {
		return rating;
	}

	public void setRating(int rating) {
		this.rating = rating;
	}
	
	

}
