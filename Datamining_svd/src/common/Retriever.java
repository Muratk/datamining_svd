package common;


import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;



/*This class allows to extract data from our data structure which contains users ratings to specific movies
 *Different methods are created to extract different types of data
 *This is useful for all the formulas we are going to use 
 */
public class Retriever {
	
	private List<Object> l;
	
	public Retriever(List<Object> l){
		
		this.l = l;
		
	}
	
	//Lists all users From Test set
	public ArrayList<Integer> getAllUsersFromTestSet(){
		
		ArrayList<Integer> usersFromTestSet = new ArrayList<Integer>();
		Map<Integer, HashMap<Integer, Integer>> testSet;
		testSet = (Map<Integer, HashMap<Integer, Integer>>)l.get(1); //l.get(1) contains the test set for users map, refer to Parser.java and see how we store Data
		
		for(Entry<Integer, HashMap<Integer, Integer>> entry: testSet.entrySet()){
			usersFromTestSet.add(entry.getKey());
		}
		return usersFromTestSet;
		
	}
	
	//Lists all items From Test set
		public ArrayList<Integer> getAllItemsFromTestSet(){
			
			ArrayList<Integer> itemsFromTestSet = new ArrayList<Integer>();
			Map<Integer, HashMap<Integer, Integer>> testSet;
			testSet = (Map<Integer, HashMap<Integer, Integer>>)l.get(3); //l.get(1) contains the test set for items map, refer to Parser.java and see how we store Data
			
			for(Entry<Integer, HashMap<Integer, Integer>> entry: testSet.entrySet()){
				itemsFromTestSet.add(entry.getKey());
			}
			return itemsFromTestSet;
			
		}
	
	//Lists all users of Training set
	public ArrayList<Integer> getAllUsersFromTrainingSet(){
		
		ArrayList<Integer> usersFromTrainingSet = new ArrayList<Integer>();
		Map<Integer, HashMap<Integer, Integer>> trainingSet = new HashMap<Integer, HashMap<Integer, Integer>>();
		trainingSet = (Map<Integer, HashMap<Integer, Integer>>)l.get(0); //l.get(0) contains the training set for users map, refer to Parser.java and see how we store Data
		
		for(Entry<Integer, HashMap<Integer, Integer>> entry: trainingSet.entrySet()){
			Integer key = entry.getKey();
			usersFromTrainingSet.add(key);
		}
		return usersFromTrainingSet;
		
	}
	
	//Lists all users Items of Training set
		public ArrayList<Integer> getAllItemsFromTrainingSet(){
			
			ArrayList<Integer> itemsFromTrainingSet = new ArrayList<Integer>();
			Map<Integer, HashMap<Integer, Integer>> trainingSet = new HashMap<Integer, HashMap<Integer, Integer>>();
			trainingSet = (Map<Integer, HashMap<Integer, Integer>>)l.get(2); //l.get(2) contains the training set for items map, refer to Parser.java and see how we store Data
			
			for(Entry<Integer, HashMap<Integer, Integer>> entry: trainingSet.entrySet()){
				Integer key = entry.getKey();
				itemsFromTrainingSet.add(key);
			}
			return itemsFromTrainingSet;
			
		}
	
	//Lists all users contained in both Test and Training Sets
	public ArrayList<Integer> getUsersOfBothSets(){
		
		ArrayList<Integer> usersFromTestSet = getAllUsersFromTestSet();
		ArrayList<Integer> usersFromTrainingSet = getAllUsersFromTrainingSet();
		
		ArrayList<Integer> usersFromBothSets = new ArrayList<Integer>();

		for(Integer i : usersFromTestSet){
			if(usersFromTrainingSet.contains(i)) {
				usersFromBothSets.add(i);
			}
			/*
			for(Integer j : usersFromTrainingSet){ 
				if(i == j){
					usersFromBothSets.add(i);
					break;
				}
				else {
					a = 1;
				}
			}
			*/
		}
		return usersFromBothSets;
	
		
	}
	
	
	//Gets a specific rating from user u to item i
	public int getUserRatingToItem(Map<Integer, HashMap<Integer, Integer>> dataSet, int user, int item){
		
		HashMap<Integer, Integer> map = new HashMap<Integer, Integer>();
		map = dataSet.get(user);
		int rate = map.get(item).intValue();
		
		return rate;
		
	}
	
	//Returns a map which contains the item rated by the user as key and the associated rating as value
	public HashMap<Integer, Integer> getUserRatings(Map<Integer, HashMap<Integer, Integer>> dataSet, int user){
		
		HashMap<Integer, Integer> userRatings = new HashMap<Integer, Integer>();
		userRatings = dataSet.get(user);
		
		return userRatings;
	}
	
	
	//Gets a map which contains all item rated by both u and v
	public HashMap<Integer, Integer> getIntersectionRatings(HashMap<Integer, Integer> map1, HashMap<Integer, Integer> map2){
		
		HashMap<Integer, Integer> result = new HashMap<Integer, Integer>(map1);
		result.keySet().retainAll(map2.keySet());
		
		return result;
		
	}
	

}	


