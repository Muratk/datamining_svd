package common;


import java.util.ArrayList;
import java.util.HashMap;

public class User {

	ArrayList<Integer> itemsRatedInTraining;
	ArrayList<Integer> itemsRatedInTest;
	ArrayList<Integer> nearestNeighbors;
	HashMap<Integer, HashMap<Integer, Integer>> itemsAssociatedToNeighbors;
	HashMap<Integer, Double> itemsRateAvgInTest;
	
	public User() {
		itemsRatedInTraining = new ArrayList<>();
		itemsRatedInTest = new ArrayList<>();
		nearestNeighbors = new ArrayList<>();
		itemsAssociatedToNeighbors = new HashMap<>();
		itemsRateAvgInTest = new HashMap<>();
	}
	
	public void setItemsRatedInTraining(ArrayList<Integer> items) {
		itemsRatedInTraining = items;
	}

	public void setItemsRatedInTest(ArrayList<Integer> items) {
		itemsRatedInTest = items;
	}
	
	public ArrayList<Integer> getItemsRatedInTest() {
		return itemsRatedInTest;
	}
	
	public void setNearestNeighbors(ArrayList<Integer> neighbors) {
		nearestNeighbors = neighbors;
	}
	
	public ArrayList<Integer> getNearestNeighbors() {
		return nearestNeighbors;
	}
	
	public void setItemsAssociatedToNeighbors(HashMap<Integer, HashMap<Integer, Integer>> items) {
		itemsAssociatedToNeighbors = items;
	}
	
	public HashMap<Integer, HashMap<Integer, Integer>> getItemsAssociatedToNeighbors() {
		return itemsAssociatedToNeighbors;
	}
	
	public void setItemsRateAvgInTest(HashMap<Integer, Double> items) {
		itemsRateAvgInTest = items;
	}
	
	public HashMap<Integer, Double> getItemsRateAvgInTest() {
		return itemsRateAvgInTest;
	}
}
