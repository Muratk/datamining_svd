package common;


import java.io.BufferedReader;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import svd.Ligne;



/*In this class we basically use one method that will put all our data from the file into a List which contains 4 HashMap :
 * 	1 - outerMapForUsersTrain : HashMap<Integer, HashMap<Integer, Integer>> the key represents the user, the
 *		 inner map contains the rated item as Integer key and the attributed rate as Integer value
 *		These are all values from the "Training Set" which represents 90% of the data, hence the "Train" in the variable name
 *
 *	2 - outerMapForUsersTest: HashMap<Integer, HashMap<Integer, Integer>> the key represents the user, the
 * 		inner map contains the rated item as Integer key and the attributed rate as Integer value
 *		These are all values from the "Test Set" which represents 90% of the data, hence the "Test" in the variable name
 *	
 *	3 - outerMapForItemTrain : This time the key is the item
 *
 *	4 - outerMapForItemTest
 *
 *
 *
 *
 *
 */

public class Parser {
	
	private String filename;
	
	public Parser(String filename){
		
		this.filename = filename;
		
	}
	
	/*The attribute "part" allows you to chose the part to "cut", you can choose from 1 to 9
	 * 1 
	 */
	
	public List<Object> parseData(double part){

		Map<Integer, HashMap<Integer, Integer>> outerMapForUsersTrain = new LinkedHashMap<Integer, HashMap<Integer, Integer>>();
		Map<Integer, HashMap<Integer, Integer>> outerMapForUsersTest = new LinkedHashMap<Integer, HashMap<Integer, Integer>>();
		
		Map<Integer, HashMap<Integer, Integer>> outerMapForItemsTrain = new LinkedHashMap<Integer, HashMap<Integer, Integer>>();
		Map<Integer, HashMap<Integer, Integer>> outerMapForItemsTest = new LinkedHashMap<Integer, HashMap<Integer, Integer>>();
		
		HashMap<Integer, Integer> innerMapForUsersTrain = new HashMap<Integer, Integer>();
		HashMap<Integer, Integer> innerMapForUsersTest = new HashMap<Integer, Integer>();
		
		HashMap<Integer, Integer> innerMapForItemsTrain = new HashMap<Integer, Integer>();
		HashMap<Integer, Integer> innerMapForItemsTest = new HashMap<Integer, Integer>();
		
		// Map<Integer, HashMap<Integer, Integer>>
//		mapArray.add(outerMapForUsersTrain);
//		mapArray.add(outerMapForUsersTest);
//		mapArray.add(outerMapForItemsTrain);
//		mapArray.add(outerMapForItemsTest);
		List<Object> objArray = new ArrayList<>();
		
		List<Ligne> listLignesTrain = new LinkedList<>();
		List<Ligne> listLignesTest = new LinkedList<>();
		
		try {
			
			BufferedReader br = new BufferedReader(new FileReader(filename));

			//Variables
			String sCurrentLine;
			String[] data;
			
			Integer user_id;
			Integer item_id;
			Integer rate;
			
			/*Declarations des maps
			
			Map<Integer, HashMap<Integer, Integer>> outerMapForItems = new HashMap<Integer, HashMap<Integer, Integer>>();
			HashMap<Integer, Integer> innerMapForItems = new HashMap<Integer, Integer>();
			*/
			
			
			int totalLines = 0;
			int lines = 0;
			BufferedReader reader = new BufferedReader(new FileReader(filename));
			while (reader.readLine() != null) totalLines++;  // we get total lines, useful for splitting
			reader.close();
			
			//To make sure that an acceptable part is chosen
			if(0 <= part && part <= 9){
				
				double bas = (part/10)*totalLines;
				double haut = ((part+1)/10)*totalLines;
			
				//File reading
				while (((sCurrentLine = br.readLine()) != null)) {
				
					//Test made to determine which part we are going to "cut" 
					if(lines >= bas && lines <= haut) {
						
						data = sCurrentLine.split("\t");
					
						user_id = Integer.parseInt(data[0]);
						item_id = Integer.parseInt(data[1]);
						rate = Integer.parseInt(data[2]);
						
						Ligne ligne = new Ligne(user_id, item_id, rate);
						listLignesTest.add(ligne);
					
						innerMapForUsersTest = outerMapForUsersTest.get(user_id);
						innerMapForItemsTest = outerMapForItemsTest.get(item_id);
					
						if(innerMapForItemsTest == null){
							innerMapForItemsTest = new HashMap<Integer, Integer>();
							innerMapForItemsTest.put(user_id, rate);
							outerMapForItemsTest.put(item_id, innerMapForItemsTest);
						}
						
						else{
							innerMapForItemsTest.put(user_id, rate);
						}
						
						if(innerMapForUsersTest == null) {
							innerMapForUsersTest = new HashMap<Integer, Integer>();
							innerMapForUsersTest.put(item_id, rate);
							outerMapForUsersTest.put(user_id, innerMapForUsersTest);
						}
					
						else {
							innerMapForUsersTest.put(item_id, rate);
						}
					
					}
					
					else {
						
						data = sCurrentLine.split("\t");
						
						user_id = Integer.parseInt(data[0]);
						item_id = Integer.parseInt(data[1]);
						rate = Integer.parseInt(data[2]);
						
						Ligne ligne = new Ligne(user_id, item_id, rate);
						listLignesTrain.add(ligne);
						
						innerMapForUsersTrain = outerMapForUsersTrain.get(user_id);
						innerMapForItemsTrain = outerMapForItemsTrain.get(item_id);
						
						if(innerMapForItemsTrain == null) {
							innerMapForItemsTrain = new HashMap<Integer, Integer>();
							innerMapForItemsTrain.put(user_id, rate);
							outerMapForItemsTrain.put(item_id, innerMapForItemsTrain);
						}
						
						else{
							innerMapForItemsTrain.put(user_id, rate);
						}
						
						if(innerMapForUsersTrain == null) {
							innerMapForUsersTrain = new HashMap<Integer, Integer>();
							innerMapForUsersTrain.put(item_id, rate);
							outerMapForUsersTrain.put(user_id, innerMapForUsersTrain);
						}
						
						else {
							innerMapForUsersTrain.put(item_id, rate);
						}
					}
					
					lines++;
					
				}
			}
				
			else{
				System.out.println("Please chose a part between 1 and 9");
			}
		
		}		
				
		catch(Exception e) {
			
			e.printStackTrace();
			
		}
		
		objArray.add(outerMapForUsersTrain);
		objArray.add(outerMapForUsersTest);
		objArray.add(outerMapForItemsTrain);
		objArray.add(outerMapForItemsTest);
		objArray.add(listLignesTrain);
		objArray.add(listLignesTest);
		
		return objArray;
	}
	
	
	
	//useless
	public Map<Integer, HashMap<Integer, Integer>> parseData(String filename) {
		
		Map<Integer, HashMap<Integer, Integer>> outerMapForUsers = new HashMap<Integer, HashMap<Integer, Integer>>();
		
		HashMap<Integer, Integer> innerMapForUsers = new HashMap<Integer, Integer>();
		
		try {
			
			BufferedReader br = new BufferedReader(new FileReader("u.data"));

			//Variables
			String sCurrentLine;
			String[] data;
			
			Integer user_id;
			Integer item_id;
			Integer rate;
			
			/*Declarations des maps
			
			Map<Integer, HashMap<Integer, Integer>> outerMapForItems = new HashMap<Integer, HashMap<Integer, Integer>>();
			HashMap<Integer, Integer> innerMapForItems = new HashMap<Integer, Integer>();
			*/
			
			//Stockage ds map
			while ((sCurrentLine = br.readLine()) != null) {
				data = sCurrentLine.split("\t");
				
				user_id = Integer.parseInt(data[0]);
				item_id = Integer.parseInt(data[1]);
				rate = Integer.parseInt(data[2]);
				
				innerMapForUsers = outerMapForUsers.get(user_id);
				
				if(innerMapForUsers == null) {
					innerMapForUsers = new HashMap<Integer, Integer>();
					innerMapForUsers.put(item_id, rate);
					outerMapForUsers.put(user_id, innerMapForUsers);
				}
				
				else {
					innerMapForUsers.put(item_id, rate);
				}
				
				
			}
		}
		
		catch(Exception e) {
			
			e.printStackTrace();
			
		}
		
		return outerMapForUsers;
			
	}

}
