<h2>About this project</h2>
This project is a Recommander System based on the SVD (Singular Values Decomposition) technique. 

<h2>Datasets</h2>
It has been tested on Movielens 100M("u.data" file) and 1M datasets.